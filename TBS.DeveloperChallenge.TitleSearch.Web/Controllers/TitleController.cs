﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBS.DeveloperChallenge.TitleSearch.Web.Services;

namespace TBS.DeveloperChallenge.TitleSearch.Web.Controllers
{
    public class TitleController : Controller
    {
   
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(string id)
        {
            var ts = new TitleService();

            return View(ts.GetTitle(id));
        }

        [HttpGet]
        public ActionResult Search(string query = "")
        {
            var ts = new TitleService();

            var results = ts.SearchTitles(query.ToLower());
            return Json(results, JsonRequestBehavior.AllowGet);
        }

    }
}
