﻿using System.Configuration;
using MongoDB.Driver;


namespace TBS.DeveloperChallenge.TitleSearch.Web.Helpers
{
    public class MongoHelper<T> where T : class
    {
        public MongoCollection<T> Collection { get; set; }

        public MongoHelper()
        {
            var url = ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString;

            var client = new MongoClient(url);
            var server = client.GetServer();
            var db = server.GetDatabase("dev-test");

            Collection = db.GetCollection<T>("titles");
        }
    }

}