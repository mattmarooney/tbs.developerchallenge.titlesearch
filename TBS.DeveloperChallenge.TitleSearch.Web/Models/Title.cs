﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TBS.DeveloperChallenge.TitleSearch.Web.Models
{
    public class OtherNames
    {
        public string TitleNameLanguage { get; set; }
        public string TitleNameType { get; set; }
        public string TitleNameSortable { get; set; }
        public string TitleName { get; set; }
    }

    public class Storylines
    {
        public string Description { get; set; }
        public string Language { get; set; }
        public string Type { get; set; }
    }

    public class Participants
    {
        public bool IsKey { get; set; }
        public string RoleType { get; set; }
        public bool IsOnScreen { get; set; }
        public string ParticipantType { get; set; }
        public string Name { get; set; }
        public int ParticipantId { get; set; }
        public int? SortOrder { get; set; }
    }

    public class Awards
    {
        public bool AwardWon { get; set; }
        public int AwardYear { get; set; }
        public List<string> Participants { get; set; }
        public string Award { get; set; }
        public string AwardCompany { get; set; }
    }

    public class ExternalSources
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }

    public class RatingDescriptors
    {
        public string Rating { get; set; }
        public List<string> Descriptors { get; set; }
        public string NetworkCode { get; set; }
    }

    public class Ratings
    {
        public List<RatingDescriptors> RatingDescriptors { get; set; }
        public string RatingSystem { get; set; }
    }

    public class RelatedTitles
    {
        public string AssociatedTitleType { get; set; }
        public string AssociatedTitleName { get; set; }
        public int AssociatedTitleId { get; set; }
        public string Relationship { get; set; }
    }

    public class Title
    {
        [BsonId]
        public string _id { get; set; }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public string TitleType { get; set; }
        public string TitleTypeCode { get; set; }
        public int ReleaseYear { get; set; }
        public string PerformanceMode { get; set; }
        public string AnimationMode { get; set; }
        public string ProcessedDatetimeUTC { get; set; }
        public List<string> Genres { get; set; }
        public List<OtherNames> OtherNames { get; set; }
        public List<Storylines> Storylines { get; set; }
        public List<Participants> Participants { get; set; }
        public List<Awards> Awards { get; set; }
        public List<string> NetworkLevels { get; set; }
        public List<ExternalSources> ExternalSources { get; set; }
        public List<string> Keywords { get; set; }
        public List<Ratings> Ratings { get; set; }
        public List<RelatedTitles> RelatedTitles { get; set; }
        public List<string> KeyGenres { get; set; }
    }

    
}