﻿using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using TBS.DeveloperChallenge.TitleSearch.Web.Helpers;
using TBS.DeveloperChallenge.TitleSearch.Web.Models;

namespace TBS.DeveloperChallenge.TitleSearch.Web.Services
{
    public class TitleService
    {
        private readonly MongoHelper<Title> _titles;

        public TitleService()
        {
            _titles = new MongoHelper<Title>();
        }

        public List<Title> SearchTitles(string term)
        {
            //"ix" performs case-insensitive search ignoring whitespace
            var titles = _titles.Collection.Find(Query.Matches("TitleName", new BsonRegularExpression(term, "i")));
            return titles.ToList();
        }

        public Title GetTitle(string id)
        {
            return _titles.Collection.FindOneById(BsonValue.Create(id));
        }
    }
}